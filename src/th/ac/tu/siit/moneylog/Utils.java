package th.ac.tu.siit.moneylog;

import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

public class Utils {
	
    public static boolean  inflateMenu(Activity activity, Menu menu) {
    	MenuInflater inflater = activity.getMenuInflater();
		inflater.inflate( R.menu.main, menu);
		return true; 
    }
    
    public static boolean handleMenuOption(Activity activity, MenuItem item) {
    	Intent intent;
		switch(item.getItemId()) {
		case R.id.optAddAccount :
			  intent = new Intent(activity,AddCategory.class);
			  activity.startActivity(intent);
			  break;
		case R.id.optAddTransaction :
			  intent = new Intent(activity,AddPayment.class);
			  activity.startActivity(intent);
			  break;
			  
		case R.id.optListAccounts :
			  intent = new Intent(activity,ListCategory.class);
			  activity.startActivity(intent);
			  break;			  
			  
		}
		return true;
    }
    
}

