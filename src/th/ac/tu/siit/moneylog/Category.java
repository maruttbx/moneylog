package th.ac.tu.siit.moneylog;

public class Category {
  private String id,acno,bank,branch,holder;

public String getId() {
	return id;
}

public void setId(String id) {
	this.id = id;
}

public String getBank() {
	return bank;
}

public void setBank(String bank) {
	this.bank = bank;
}

public String getHolder() {
	return holder;
}

public void setHolder(String holder) {
	this.holder = holder;
}

@Override
public String toString() {
	return  holder + " " + bank;
}
  
}
