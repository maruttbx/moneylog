package th.ac.tu.siit.moneylog;

import android.app.Activity;
import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class AddCategory extends Activity {
	
	 @Override
	    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        setContentView(R.layout.addcategory);
	    }
	 
	 
	 @Override
		public boolean onCreateOptionsMenu(Menu menu) {
			return Utils.inflateMenu(this,menu);
		}
		
		@Override 
		public boolean onOptionsItemSelected(MenuItem item) {
			return  Utils.handleMenuOption(this,item);
		}
		
	  public void addAccount(View v) {
		    EditText editHolders = (EditText) this.findViewById(R.id.editHolders);
		    EditText editBankName = (EditText) this.findViewById(R.id.editBankName);
		    EditText editBalance = (EditText) this.findViewById(R.id.editBalance);
		    
			try {
				DBHelper dbhelper = new DBHelper(this); 
				SQLiteDatabase db = dbhelper.getWritableDatabase();
                Log.d("Account","Got Writable database");


				ContentValues values = new ContentValues();
				values.put( Database.ACCOUNTS_HOLDERS, editHolders.getText().toString());
				values.put( Database.ACCOUNTS_BANK, editBankName.getText().toString());
				values.put( Database.ACCOUNTS_BALANCE, editBalance.getText().toString());
				

				long rows = db.insert(Database.ACCOUNTS_TABLE_NAME, null, values);
				db.close();
				if ( rows > 0)  {
				    Toast.makeText(this, "Added Successfully!",	Toast.LENGTH_LONG).show();
				    this.finish();
				}
				else
					Toast.makeText(this, "Sorry! Could not add this!",	Toast.LENGTH_LONG).show();
				
			} catch (Exception ex) {
				Toast.makeText(this, ex.getMessage(), Toast.LENGTH_LONG).show();
			}

		  
	  }

}
